/**
 * \file
 *
 * \brief SAM3X-IRBIS Board Definition.
 *
 * Copyright (c) 2011 - 2013 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef _SAM3X_EK_H_
#define _SAM3X_EK_H_

#include "compiler.h"
#include "system_sam3x.h"
#include "exceptions.h"

/*#define BOARD_REV_A */
#define BOARD_REV_B

/* ------------------------------------------------------------------------ */

/**
 *  \page sam3x_ek_opfreq "SAM3X-IRBIS - Operating frequencies"
 *  This page lists several definition related to the board operating frequency
 *
 *  \section Definitions
 *  - \ref BOARD_FREQ_*
 *  - \ref BOARD_MCK
 */

/*! Board oscillator settings */
#define BOARD_FREQ_SLCK_XTAL            (32768U)
#define BOARD_FREQ_SLCK_BYPASS          (32768U)
#define BOARD_FREQ_MAINCK_XTAL          (12000000U)
#define BOARD_FREQ_MAINCK_BYPASS        (12000000U)

/*! Master clock frequency */
#define BOARD_MCK                       CHIP_FREQ_CPU_MAX

/** board main clock xtal statup time */
#define BOARD_OSC_STARTUP_US   15625

/* ------------------------------------------------------------------------ */

/**
 * \page sam3x_ek_board_info "SAM3X-IRBIS - Board informations"
 * This page lists several definition related to the board description.
 *
 * \section Definitions
 * - \ref BOARD_NAME
 */

/*! Name of the board */
#define BOARD_NAME "SAM3X-IRBIS"
/*! Board definition */
#define sam3xek
/*! Family definition (already defined) */
#define sam3x
/*! Core definition */
#define cortexm3

/* ------------------------------------------------------------------------ */

/**
 * \page sam3x_ek_piodef "SAM3X-IRBIS - PIO definitions"
 * This pages lists all the pio definitions. The constants
 * are named using the following convention: PIN_* for a constant which defines
 * a single Pin instance (but may include several PIOs sharing the same
 * controller), and PINS_* for a list of Pin instances.
 *
 */

/**
 * \file
 * ADC
 */

/*! ADC pin definition. */
#define PINS_ADC_TRIG  PIO_PA11_IDX
#define PINS_ADC_TRIG_FLAG  (PIO_PERIPH_B | PIO_DEFAULT)


/**
 * \file
 * LEDs
 * - \ref PIN_USER_LED0
 * - \ref PIN_USER_LED1
 *
 */

/* ------------------------------------------------------------------------ */
/* LEDS                                                                     */
/* ------------------------------------------------------------------------ */
/*! LED #0 pin definition (GREEN). */
#define LED_0_NAME      "green LED"
#define LED0_GPIO       (PIO_PC21_IDX)
#define LED0_FLAGS      (PIO_TYPE_PIO_OUTPUT_1 | PIO_DEFAULT)
#define LED0_ACTIVE_LEVEL 0

#define PIN_LED_0       {PIO_PC21, PIOC, ID_PIOC, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_LED_0_MASK  PIO_PC21
#define PIN_LED_0_PIO   PIOC
#define PIN_LED_0_ID    ID_PIOC
#define PIN_LED_0_TYPE  PIO_OUTPUT_1
#define PIN_LED_0_ATTR  PIO_DEFAULT

#define LED0_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/*! LED #1 pin definition (RED). */
#define LED_1_NAME      "red LED"
#define LED1_GPIO       (PIO_PC22_IDX)
#define LED1_FLAGS      (PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)
#define LED1_ACTIVE_LEVEL 0

#define PIN_LED_1       {PIO_PC22, PIOC, ID_PIOC, PIO_OUTPUT_1, PIO_DEFAULT}
#define PIN_LED_1_MASK  PIO_PC22
#define PIN_LED_1_PIO   PIOC
#define PIN_LED_1_ID    ID_PIOC
#define PIN_LED_1_TYPE  PIO_OUTPUT_1
#define PIN_LED_1_ATTR  PIO_DEFAULT

#define LED1_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH


#define BOARD_NUM_OF_LED 2




/**
 * \file
 * RELAYs
 * - \ref PIN_USER_RELAY0
 * - \ref PIN_USER_RELAY1
 * - \ref PIN_USER_RELAY2
 * - \ref PIN_USER_RELAY3
 *
 */

/* ------------------------------------------------------------------------ */
/* RELAYs                                                                     */
/* ------------------------------------------------------------------------ */
/*! RELAY #0 pin definition. */
#define RELAY0_GPIO       (PIO_PC5_IDX)
#define RELAY0_FLAGS      (PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)
#define RELAY0_ACTIVE_LEVEL 0

#define PIN_RELAY_0       {PIO_PC5, PIOC, ID_PIOC, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_RELAY_0_MASK  PIO_PC5
#define PIN_RELAY_0_PIO   PIOC
#define PIN_RELAY_0_ID    ID_PIOC
#define PIN_RELAY_0_TYPE  PIO_OUTPUT_1
#define PIN_RELAY_0_ATTR  PIO_DEFAULT

#define RELAY0_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/*! RELAY #1 pin definition. */
#define RELAY1_GPIO       (PIO_PC6_IDX)
#define RELAY1_FLAGS      (PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)
#define RELAY1_ACTIVE_LEVEL 0

#define PIN_RELAY_1       {PIO_PC6, PIOC, ID_PIOC, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_RELAY_1_MASK  PIO_PC6
#define PIN_RELAY_1_PIO   PIOC
#define PIN_RELAY_1_ID    ID_PIOC
#define PIN_RELAY_1_TYPE  PIO_OUTPUT_1
#define PIN_RELAY_1_ATTR  PIO_DEFAULT

#define RELAY1_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/*! RELAY #2 pin definition. */
#define RELAY2_GPIO       (PIO_PC7_IDX)
#define RELAY2_FLAGS      (PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)
#define RELAY2_ACTIVE_LEVEL 0

#define PIN_RELAY_2       {PIO_PC7, PIOC, ID_PIOC, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_RELAY_2_MASK  PIO_PC7
#define PIN_RELAY_2_PIO   PIOC
#define PIN_RELAY_2_ID    ID_PIOC
#define PIN_RELAY_2_TYPE  PIO_OUTPUT_1
#define PIN_RELAY_2_ATTR  PIO_DEFAULT

#define RELAY2_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/*! RELAY #3 pin definition. */
#define RELAY3_GPIO       (PIO_PC8_IDX)
#define RELAY3_FLAGS      (PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)
#define RELAY3_ACTIVE_LEVEL 0

#define PIN_RELAY_3       {PIO_PC8, PIOC, ID_PIOC, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_RELAY_3_MASK  PIO_PC8
#define PIN_RELAY_3_PIO   PIOC
#define PIN_RELAY_3_ID    ID_PIOC
#define PIN_RELAY_3_TYPE  PIO_OUTPUT_1
#define PIN_RELAY_3_ATTR  PIO_DEFAULT

#define RELAY3_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH


#define BOARD_NUM_OF_RELAY 4






/*! COUNT pins definition. */
#define COUNT1_GPIO       (PIO_PC29_IDX)
#define COUNT2_GPIO       (PIO_PC19_IDX)
#define COUNT3_GPIO       (PIO_PC18_IDX)
#define COUNT4_GPIO       (PIO_PC17_IDX)
#define COUNT5_GPIO       (PIO_PC16_IDX)
#define COUNT6_GPIO       (PIO_PC15_IDX)

#define COUNT_FLAGS      (PIO_INPUT | PIO_PULLUP)



/**
 * \file
 * SPI
 *
 */

/* ------------------------------------------------------------------------ */
/* SPI                                                                      */
/* ------------------------------------------------------------------------ */
/*! SPI0 MISO pin definition. */
#define SPI0_MISO_GPIO        (PIO_PA25_IDX)
#define SPI0_MISO_FLAGS       (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI0 MOSI pin definition. */
#define SPI0_MOSI_GPIO        (PIO_PA26_IDX)
#define SPI0_MOSI_FLAGS       (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI0 SPCK pin definition. */
#define SPI0_SPCK_GPIO        (PIO_PA27_IDX)
#define SPI0_SPCK_FLAGS       (PIO_PERIPH_A | PIO_DEFAULT)

/*! SPI0 chip select 0 pin definition. (Only one configuration is possible) */
#define SPI0_NPCS0_GPIO            (PIO_PA28_IDX)
#define SPI0_NPCS0_FLAGS           (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI0 chip select 1 pin definition. (multiple configurations are possible) */
#define SPI0_NPCS1_PA29_GPIO       (PIO_PA29_IDX)
#define SPI0_NPCS1_PA29_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI0_NPCS1_PB20_GPIO       (PIO_PB20_IDX)
#define SPI0_NPCS1_PB20_FLAGS      (PIO_PERIPH_B | PIO_DEFAULT)
/*! SPI0 chip select 2 pin definition. (multiple configurations are possible) */
#define SPI0_NPCS2_PA30_GPIO       (PIO_PA30_IDX)
#define SPI0_NPCS2_PA30_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI0_NPCS2_PB21_GPIO       (PIO_PB21_IDX)
#define SPI0_NPCS2_PB21_FLAGS      (PIO_PERIPH_B | PIO_DEFAULT)
/*! SPI0 chip select 3 pin definition. (multiple configurations are possible) */
#define SPI0_NPCS3_PA31_GPIO       (PIO_PA31_IDX)
#define SPI0_NPCS3_PA31_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI0_NPCS3_PB23_GPIO       (PIO_PB23_IDX)
#define SPI0_NPCS3_PB23_FLAGS      (PIO_PERIPH_B | PIO_DEFAULT)

/*! SPI1 MISO pin definition. */
#define SPI1_MISO_GPIO             (PIO_PE28_IDX)
#define SPI1_MISO_FLAGS            (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI1 MOSI pin definition. */
#define SPI1_MOSI_GPIO             (PIO_PE29_IDX)
#define SPI1_MOSI_FLAGS            (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI1 SPCK pin definition. */
#define SPI1_SPCK_GPIO             (PIO_PE30_IDX)
#define SPI1_SPCK_FLAGS            (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI1 chip select 0 pin definition. (Only one configuration is possible) */
#define SPI1_NPCS0_GPIO            (PIO_PE31_IDX)
#define SPI1_NPCS0_FLAGS           (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI1 chip select 1 pin definition. (Only one configuration is possible) */
#define SPI1_NPCS1_GPIO            (PIO_PF0_IDX)
#define SPI1_NPCS1_FLAGS           (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI1 chip select 2 pin definition. (Only one configuration is possible) */
#define SPI1_NPCS2_GPIO            (PIO_PF1_IDX)
#define SPI1_NPCS2_FLAGS           (PIO_PERIPH_A | PIO_DEFAULT)
/*! SPI1 chip select 3 pin definition. (Only one configuration is possible) */
#define SPI1_NPCS3_GPIO            (PIO_PF2_IDX)
#define SPI1_NPCS3_FLAGS           (PIO_PERIPH_A | PIO_DEFAULT)

/**
 * \file
 * PCK0
 * - \ref PIN_PCK0
 *
 */

/* ------------------------------------------------------------------------ */
/* PCK                                                                      */
/* ------------------------------------------------------------------------ */
/*! PCK0 */
#define PIN_PCK0        (PIO_PB22_IDX)
#define PIN_PCK0_MUX    (IOPORT_MODE_MUX_B)
#define PIN_PCK0_FLAGS  (PIO_PERIPH_B | PIO_DEFAULT)

#define PIN_PCK_0_MASK  PIO_PB22
#define PIN_PCK_0_PIO   PIOB
#define PIN_PCK_0_ID    ID_PIOB
#define PIN_PCK_0_TYPE  PIO_PERIPH_B
#define PIN_PCK_0_ATTR  PIO_DEFAULT

/**
 * \file
 * HSMCI
 * - \ref PINS_HSMCI
 *
 */

/* ------------------------------------------------------------------------ */
/* HSMCI                                                                      */
/* ------------------------------------------------------------------------ */
/*! HSMCI pins definition. */
/*! Number of slot connected on HSMCI interface */
#define SD_MMC_HSMCI_MEM_CNT        1
#define SD_MMC_HSMCI_SLOT_0_SIZE    4


/** HSMCI MCCDA pin definition. */
#define PIN_HSMCI_MCCDA_GPIO            (PIO_PA20_IDX)
#define PIN_HSMCI_MCCDA_FLAGS           (PIO_PULLUP |PIO_PERIPH_A | PIO_DEFAULT)
/** HSMCI MCCK pin definition. */
#define PIN_HSMCI_MCCK_GPIO             (PIO_PA19_IDX)
#define PIN_HSMCI_MCCK_FLAGS            (PIO_PERIPH_A | PIO_DEFAULT)
/** HSMCI MCDA0 pin definition. */
#define PIN_HSMCI_MCDA0_GPIO            (PIO_PA21_IDX)
#define PIN_HSMCI_MCDA0_FLAGS           (PIO_PULLUP | PIO_PERIPH_A | PIO_DEFAULT)
/** HSMCI MCDA1 pin definition. */
#define PIN_HSMCI_MCDA1_GPIO            (PIO_PA22_IDX)
#define PIN_HSMCI_MCDA1_FLAGS           (PIO_PULLUP | PIO_PERIPH_A | PIO_DEFAULT)
/** HSMCI MCDA2 pin definition. */
#define PIN_HSMCI_MCDA2_GPIO            (PIO_PA23_IDX)
#define PIN_HSMCI_MCDA2_FLAGS           (PIO_PULLUP | PIO_PERIPH_A | PIO_DEFAULT)
/** HSMCI MCDA3 pin definition. */
#define PIN_HSMCI_MCDA3_GPIO            (PIO_PA24_IDX)
#define PIN_HSMCI_MCDA3_FLAGS           (PIO_PULLUP | PIO_PERIPH_A | PIO_DEFAULT)

/** SD/MMC card detect pin definition. */
#define PIN_HSMCI_CD {PIO_PA18, PIOA, ID_PIOA, PIO_INPUT, PIO_PULLUP}
#define SD_MMC_0_CD_GPIO            (PIO_PA18_IDX)
#define SD_MMC_0_CD_PIO_ID          ID_PIOA
#define SD_MMC_0_CD_FLAGS           (PIO_INPUT | PIO_PULLUP)
#define SD_MMC_0_CD_DETECT_VALUE    0

/**
 * \file
 * USART0 - GSM Modem
 * - \ref PIN_USART0_RXD
 * - \ref PIN_USART0_TXD
 * - \ref PIN_USART0_CTS
 * - \ref PIN_USART0_RTS
 *
 */

/* ------------------------------------------------------------------------ */
/* USART0                                                                   */
/* ------------------------------------------------------------------------ */
/*! USART0 pin RX */
#define PIN_USART0_RXD\
	{PIO_PA10A_RXD0, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_RXD_IDX        (PIO_PA10_IDX)
#define PIN_USART0_RXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART0 pin TX */
#define PIN_USART0_TXD\
	{PIO_PA11A_TXD0, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_TXD_IDX        (PIO_PA11_IDX)
#define PIN_USART0_TXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART0 pin CTS */
#define PIN_USART0_CTS\
	{PIO_PB26A_CTS0, PIOB, ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_CTS_IDX        (PIO_PB26_IDX)
#define PIN_USART0_CTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART0 pin RTS */
#define PIN_USART0_RTS\
	{PIO_PB25A_RTS0, PIOB, ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_RTS_IDX        (PIO_PB25_IDX)
#define PIN_USART0_RTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)

#define USART_GSM	USART0

/*! Modem power on */
#define PIN_MODEM_ON\
	{PIO_PA9, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_MODEM_ON_IDX			(PIO_PA9_IDX)
#define PIN_MODEM_ON_FLAGS			(PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)

/*! Modem reset */
#define PIN_MODEM_RESET\
	{PIO_PD9, PIOD, ID_PIOD, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_MODEM_RESET_IDX			(PIO_PD9_IDX)
#define PIN_MODEM_RESET_FLAGS		(PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)

/*! Modem status */
#define PIN_MODEM_STATUS\
	{PIO_PD8, PIOD, ID_PIOD, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_MODEM_STATUS_IDX		(PIO_PD8_IDX)
#define PIN_MODEM_STATUS_FLAGS		(PIO_TYPE_PIO_INPUT | PIO_DEFAULT)

/*! Modem JDR */
#define PIN_MODEM_JDR\
	{PIO_PD7, PIOD, ID_PIOD, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_MODEM_JDR_IDX		(PIO_PD7_IDX)
#define PIN_MODEM_JDR_FLAGS		(PIO_TYPE_PIO_INPUT | PIO_DEFAULT) //TODO:OUT?

/**
 * \file
 * USART1 - XBee
 * - \ref PIN_USART1_RXD
 * - \ref PIN_USART1_TXD
 * - \ref PIN_USART1_CTS
 * - \ref PIN_USART1_RTS
 * - \ref PIN_USART1_DTR ??? as gpio
 *
 */

/* ------------------------------------------------------------------------ */
/* USART1                                                                   */
/* ------------------------------------------------------------------------ */
/*! USART1 pin RX */
#define PIN_USART1_RXD\
	{PIO_PA12A_RXD1, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART1_RXD_IDX        (PIO_PA12_IDX)
#define PIN_USART1_RXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART1 pin TX */
#define PIN_USART1_TXD\
	{PIO_PA13A_TXD1, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART1_TXD_IDX        (PIO_PA13_IDX)
#define PIN_USART1_TXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART1 pin CTS */
#define PIN_USART1_CTS\
	{PIO_PA15A_CTS1, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART1_CTS_IDX        (PIO_PA15_IDX)
#define PIN_USART1_CTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART1 pin RTS */
#define PIN_USART1_RTS\
	{PIO_PA14A_RTS1, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART1_RTS_IDX        (PIO_PA14_IDX)
#define PIN_USART1_RTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)

/*! USART1 pin DTR */ //??????
#define PIN_USART1_DTR\
	{PIO_PA17, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART1_DTR_IDX        (PIO_PA17_IDX)
#define PIN_USART1_DTR_FLAGS      (PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)

#define USART_XBEE	USART1

/*! XBee power on */
#define PIN_XBEE_ON\
	{PIO_PD1, PIOD, ID_PIOD, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_XBEE_ON_IDX			(PIO_PD1_IDX)
#define PIN_XBEE_ON_FLAGS		(PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)

/**
 * \file
 * USART2 - RS485
 * - \ref PIN_USART2_RXD
 * - \ref PIN_USART2_TXD
 * - \ref PIN_USART2_CTS
 * - \ref PIN_USART2_RTS
 *
 */

/* ------------------------------------------------------------------------ */
/* USART2                                                                   */
/* ------------------------------------------------------------------------ */
/*! USART2 pin RX */
#define PIN_USART2_RXD\
	{PIO_PB21A_RXD2, PIOB, ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART2_RXD_IDX        (PIO_PB21_IDX)
#define PIN_USART2_RXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART2 pin TX */
#define PIN_USART2_TXD\
	{PIO_PB20A_TXD2, PIOB, ID_PIOB, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_USART2_TXD_IDX        (PIO_PB20_IDX)
#define PIN_USART2_TXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART2 pin CTS */
#define PIN_USART2_CTS\
	{PIO_PB23A_CTS2, PIOB, ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART2_CTS_IDX        (PIO_PB23_IDX)
#define PIN_USART2_CTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART2 pin RTS */
#define PIN_USART2_RTS\
	{PIO_PB22A_RTS2, PIOB, ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART2_RTS_IDX        (PIO_PB22_IDX)
#define PIN_USART2_RTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)

#define USART_RS485	USART2


/**
 * \file
 * USART3 no use
 * - \ref PIN_USART3_RXD
 * - \ref PIN_USART3_TXD
 * - \ref PIN_USART3_CTS
 * - \ref PIN_USART3_RTS
 * - \ref PIN_USART3_SCK
 *
 */

/* ------------------------------------------------------------------------ */
/* USART3                                                                   */
/* ------------------------------------------------------------------------ */
/*! USART3 pin RX */
#define PIN_USART3_RXD\
	{PIO_PD5B_RXD3, PIOD, ID_PIOD, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_USART3_RXD_IDX        (PIO_PD5_IDX)
#define PIN_USART3_RXD_FLAGS      (PIO_PERIPH_B | PIO_DEFAULT)
/*! USART3 pin TX */
#define PIN_USART3_TXD\
	{PIO_PD4B_TXD3, PIOD, ID_PIOD, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_USART3_TXD_IDX        (PIO_PD4_IDX)
#define PIN_USART3_TXD_FLAGS      (PIO_PERIPH_B | PIO_DEFAULT)
/*! USART3 pin CTS */
#define PIN_USART3_CTS\
	{PIO_PF4A_CTS3, PIOF, ID_PIOF, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART3_CTS_IDX        (PIO_PF4_IDX)
#define PIN_USART3_CTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART3 pin RTS */
#define PIN_USART3_RTS\
	{PIO_PF5A_RTS3, PIOF, ID_PIOF, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART3_RTS_IDX        (PIO_PF5_IDX)
#define PIN_USART3_RTS_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/*! USART3 pin SCK */
#define PIN_USART3_SCK\
	{PIO_PE16B_SCK3, PIOE, ID_PIOE, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_USART3_SCK_IDX        (PIO_PE16_IDX)
#define PIN_USART3_SCK_FLAGS      (PIO_PERIPH_B | PIO_DEFAULT)

/**
 * \file
 * USB
 * - \ref PIN_USBOTG_VBOF
 *
 */

/* ------------------------------------------------------------------------ */
/* USB                                                                      */
/* ------------------------------------------------------------------------ */
/*! Multiplexed pin used for USB ID pin: */
#define USB_ID_GPIO                 (PIO_PB11_IDX)
#define USB_ID_FLAGS                (PIO_PERIPH_A | PIO_PULLUP)


/* ------------------------------------------------------------------------ */

/**
 * \page sam3x_ek_usb "SAM3X-IRBIS - USB device"
 *
 * \section Definitions
 * - \ref BOARD_USB_BMATTRIBUTES
 * - \ref CHIP_USB_UDP
 * - \ref CHIP_USB_PULLUP_INTERNAL
 * - \ref CHIP_USB_NUMENDPOINTS
 * - \ref CHIP_USB_ENDPOINTS_MAXPACKETSIZE
 * - \ref CHIP_USB_ENDPOINTS_BANKS
 */

/*! USB attributes configuration descriptor (bus or self powered, remote wakeup) */
#define BOARD_USB_BMATTRIBUTES\
	USBConfigurationDescriptor_SELFPOWERED_RWAKEUP

/*! Indicates chip has an internal pull-up. */
#define CHIP_USB_PULLUP_INTERNAL

/*! Number of USB endpoints */
#define CHIP_USB_NUMENDPOINTS 10

/*! Endpoints max packet size */
#define CHIP_USB_ENDPOINTS_MAXPACKETSIZE(i)\
	((i == 0) ? 64 : 1024)

/*! Endpoints Number of Bank */
#define CHIP_USB_ENDPOINTS_BANKS(i)\
	((i == 0) ? 1 :\
	((i == 1) ? 3 :\
	((i == 2) ? 3 : 2)))

/**
 * \file
 * CAN
 * \ref PIN_CAN0_TXD
 * \ref PIN_CAN0_RXD
 * \ref PINS_CAN0
 *
 * \ref PIN_CAN1_TXD
 * \ref PIN_CAN1_RXD
 * \ref PINS_CAN1
 */

/* ------------------------------------------------------------------------ */
/* CAN                                                                      */
/* ------------------------------------------------------------------------ */
/** CAN0 PIN RX. */
#define PIN_CAN0_RX_IDX           (PIO_PA1_IDX)
#define PIN_CAN0_RX_FLAGS         (PIO_PERIPH_A | PIO_DEFAULT)

/** CAN0 PIN TX. */
#define PIN_CAN0_TX_IDX           (PIO_PA0_IDX)
#define PIN_CAN0_TX_FLAGS         (PIO_PERIPH_A | PIO_DEFAULT)


/** CAN1 PIN RX. */
#define PIN_CAN1_RX_IDX           (PIO_PB15_IDX)
#define PIN_CAN1_RX_FLAGS         (PIO_PERIPH_A | PIO_DEFAULT)

/** CAN1 PIN TX. */
#define PIN_CAN1_TX_IDX           (PIO_PB14_IDX)
#define PIN_CAN1_TX_FLAGS         (PIO_PERIPH_A | PIO_DEFAULT)

/**
 * \file
 * TWI
 */

/* ------------------------------------------------------------------------ */
/* TWI                                                                      */
/* ------------------------------------------------------------------------ */
/*! TWI0 pins definition */
#define TWI0_DATA_GPIO   PIO_PA17_IDX
#define TWI0_DATA_FLAGS  (PIO_PERIPH_A | PIO_DEFAULT)
#define TWI0_CLK_GPIO    PIO_PA18_IDX
#define TWI0_CLK_FLAGS   (PIO_PERIPH_A | PIO_DEFAULT)

/*! TWI1 pins definition */
#define TWI1_DATA_GPIO   PIO_PB12_IDX
#define TWI1_DATA_FLAGS  (PIO_PERIPH_A | PIO_DEFAULT)
#define TWI1_CLK_GPIO    PIO_PB13_IDX
#define TWI1_CLK_FLAGS   (PIO_PERIPH_A | PIO_DEFAULT)

/* ------------------------------------------------------------------------ */

/**
 * \page sam3x_ek_extcomp "SAM3X-IRBIS - External components"
 * This page lists the definitions related to external on-board components
 * located in the board.h file for the SAM3X-IRBIS.
 *
 */




/**
 * \file
 * EMAC
 *
 * - BOARD_EMAC_PHY_ADDR: Phy MAC address
 * - BOARD_EMAC_MODE_RMII: Enable RMII connection with the PHY
 */
#define PIN_EMAC_EREFCK			PIO_PB0_IDX
#define PIN_EMAC_EREFCK_FLAGS   (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_ETXEN			PIO_PB1_IDX
#define PIN_EMAC_ETXEN_FLAGS    (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_ETX0			PIO_PB2_IDX
#define PIN_EMAC_ETX0_FLAGS     (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_ETX1			PIO_PB3_IDX
#define PIN_EMAC_ETX1_FLAGS     (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_ECRSDV			PIO_PB4_IDX
#define PIN_EMAC_ECRSDV_FLAGS   (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_ERX0			PIO_PB5_IDX
#define PIN_EMAC_ERX0_FLAGS     (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_ERX1			PIO_PB6_IDX
#define PIN_EMAC_ERX1_FLAGS     (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_ERXER			PIO_PB7_IDX
#define PIN_EMAC_ERXER_FLAGS    (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_EMDC			PIO_PB8_IDX
#define PIN_EMAC_EMDC_FLAGS     (PIO_PERIPH_A | PIO_DEFAULT)
#define PIN_EMAC_EMDIO			PIO_PB9_IDX
#define PIN_EMAC_EMDIO_FLAGS    (PIO_PERIPH_A | PIO_DEFAULT)

#define PIN_ETH_RESET           PIO_PC4_IDX
#define PIN_ETH_RESET_FLAGS     (PIO_OUTPUT_1 | PIO_DEFAULT)

#define PIN_ETH_INT             PIO_PA5_IDX
#define PIN_ETH_INT_FLAGS       (PIO_PULLUP | PIO_INPUT | PIO_DEFAULT)

/** EMAC PHY address */
#define BOARD_EMAC_PHY_ADDR  6
/*! EMAC RMII mode */
#define BOARD_EMAC_MODE_RMII 1


/* ------------------------------------------------------------------------ */

/**
 * \page sam3x_ek_mem "SAM3X-IRBIS - Memories"
 * This page lists definitions related to internal & external on-board memories.
 *
 *
 */



/* AT24CXX device address */
#define BOARD_AT24C_ADDRESS         0x50
/** Define 24LC16 TWI instance. */
#define BOARD_AT24C_TWI_INSTANCE    (TWI1)

#define EEPROM_BLOCK_SIZE	(256)
#define EEPROM_PAGE_SIZE	(16)

/*! TWI ID for EEPROM application to use */
#define BOARD_ID_TWI_EEPROM         ID_TWI1
/*! TWI Base for TWI EEPROM application to use */
#define BOARD_BASE_TWI_EEPROM       (BOARD_AT24C_TWI_INSTANCE)
/*! TWI Data pin for EEPROM */
#define BOARD_DATA_TWI_EEPROM       TWI1_DATA_GPIO
#define BOARD_DATA_TWI_MUX_EEPROM   IOPORT_MODE_MUX_A
/*! TWI Clock pin for EEPROM */
#define BOARD_CLK_TWI_EEPROM        TWI1_CLK_GPIO
#define BOARD_CLK_TWI_MUX_EEPROM    IOPORT_MODE_MUX_A



#endif  /* _SAM3X_EK_H_ */
