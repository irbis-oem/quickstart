/*
 * tests.h
 *
 * Created: 30.10.2013 14:25:57
 *  Author: Admin
 */ 


#ifndef TESTS_H_
#define TESTS_H_


#define LED_RED() \
	LED_On(LED1_GPIO); \
	LED_Off(LED0_GPIO)
	
#define LED_GREEN() \
	LED_Off(LED1_GPIO); \
	LED_On(LED0_GPIO)

#define LED_ORANGE() \
	LED_On(LED1_GPIO); \
	LED_On(LED0_GPIO)	

#define LED_BLACK() \
	LED_Off(LED1_GPIO); \
	LED_Off(LED0_GPIO)
	
#define USART_XBEE_DTR_OFF() \
	gpio_set_pin_low(PIN_USART1_DTR_IDX)
#define USART_XBEE_DTR_ON() \
	gpio_set_pin_high(PIN_USART1_DTR_IDX)

#define XBEE_POWER_OFF() \
	gpio_set_pin_low(PIN_XBEE_ON_IDX)
#define XBEE_POWER_ON() \
	gpio_set_pin_high(PIN_XBEE_ON_IDX)
	
#define MODEM_POWER_OFF() \
	gpio_set_pin_low(PIN_MODEM_ON_IDX)
#define MODEM_POWER_ON() \
	gpio_set_pin_high(PIN_MODEM_ON_IDX)
#define MODEM_RESET_OFF() \
	gpio_set_pin_low(PIN_MODEM_RESET_IDX)
#define MODEM_RESET_ON() \
	gpio_set_pin_high(PIN_MODEM_RESET_IDX)
	
#define ETH_PHY_RESET_OFF() \
	gpio_set_pin_high(PIN_ETH_RESET)
#define ETH_PHY_RESET_ON() \
	gpio_set_pin_low(PIN_ETH_RESET)	
	
	
	
///Debug Message
#define ENABLE_TESTLOG

#ifdef ENABLE_TESTLOG
#define TESTLOG			printf
#define TEST_GETCHAR	udi_cdc_getc
#define TEST_WRITECHAR	udi_cdc_putc
#else
#define TESTLOG(...)
#define TEST_GETCHAR(...)	0
#define TEST_WRITECHAR(...)
#endif



/*! \brief Run test LEDs.
 *
 * \param none.
 *
 * \note.
 */
void Test_leds(void);

void Test_EEPROM(void);

void Test_Relay(void);

void Test_Xbee(void);

void Test_Modem(void);

void Test_RS485(void);

void Test_CAN(void);

void Test_SD_MMC(void);

void Test_Ethernet(void);

void Test_DigitPin(void);

void Test_AnalPin(void);

void USB_BootLoader(void);

#endif /* TESTS_H_ */