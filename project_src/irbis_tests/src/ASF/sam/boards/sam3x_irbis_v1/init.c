/**
 * \file
 *
 * \brief SAM3X-EK board init.
 *
 * Copyright (c) 2011 - 2013 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include "compiler.h"
#include "board.h"
#include "conf_board.h"
#include "gpio.h"
#include "ioport.h"

void board_init(void)
{
	
#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
#endif

	/* GPIO has been deprecated, the old code just keeps it for compatibility.
	 * In new designs IOPORT is used instead.
	 * Here IOPORT must be initialized for others to use before setting up IO.
	 */
	ioport_init();
	
	sysclk_init();
	
	irq_initialize_vectors();
	cpu_irq_enable();

#ifdef CONF_LED_TEST_EN
	/* Configure User LED pins */
	gpio_configure_pin(LED0_GPIO, LED0_FLAGS);
	gpio_configure_pin(LED1_GPIO, LED1_FLAGS);
#endif

#ifdef CONF_RELAY_TEST_EN
	/* Configure RELAY pins */
	gpio_configure_pin(RELAY0_GPIO, RELAY0_FLAGS);
	gpio_configure_pin(RELAY1_GPIO, RELAY1_FLAGS);
	gpio_configure_pin(RELAY2_GPIO, RELAY2_FLAGS);
	gpio_configure_pin(RELAY3_GPIO, RELAY3_FLAGS);
#endif

#ifdef CONF_DIG_TEST_EN
	/* Configure COUNTx pins */
	gpio_configure_pin(COUNT1_GPIO, COUNT_FLAGS);
	gpio_configure_pin(COUNT2_GPIO, COUNT_FLAGS);
	gpio_configure_pin(COUNT3_GPIO, COUNT_FLAGS);
	gpio_configure_pin(COUNT4_GPIO, COUNT_FLAGS);
	gpio_configure_pin(COUNT5_GPIO, COUNT_FLAGS);
	gpio_configure_pin(COUNT6_GPIO, COUNT_FLAGS);
#endif


#ifdef CONF_ANALOG_TEST_EN

sysclk_enable_peripheral_clock(ID_ADC);
	/* ADC Trigger configuration */
	//gpio_configure_pin(PINS_ADC_TRIG, PINS_ADC_TRIG_FLAG);

#endif


#ifdef CONF_TWI1_TEST_EN
	sysclk_enable_peripheral_clock(ID_TWI1);

	gpio_configure_pin(BOARD_DATA_TWI_EEPROM, TWI1_DATA_FLAGS);
	gpio_configure_pin(BOARD_CLK_TWI_EEPROM, TWI1_CLK_FLAGS);
#endif

#ifdef CONF_USART0_TEST_EN
	sysclk_enable_peripheral_clock(ID_USART0);

	/* Configure USART RXD pin */
	gpio_configure_pin(PIN_USART0_RXD_IDX, PIN_USART0_RXD_FLAGS);
	/* Configure USART TXD pin */
	gpio_configure_pin(PIN_USART0_TXD_IDX, PIN_USART0_TXD_FLAGS);
	/* Configure USART CTS pin */
	gpio_configure_pin(PIN_USART0_CTS_IDX, PIN_USART0_CTS_FLAGS);
	/* Configure USART RTS pin */
	gpio_configure_pin(PIN_USART0_RTS_IDX, PIN_USART0_RTS_FLAGS);
	
	/* Configure Modem power on pin (as GPIO)*/
	gpio_configure_pin(PIN_MODEM_ON_IDX, PIN_MODEM_RESET_FLAGS);
	/* Configure Modem reset pin (as GPIO)*/
	gpio_configure_pin(PIN_MODEM_RESET_IDX, PIN_MODEM_RESET_FLAGS);
	/* Configure Modem status pin (as GPIO)*/
	gpio_configure_pin(PIN_MODEM_STATUS_IDX, PIN_MODEM_STATUS_FLAGS);
	/* Configure Modem JDR pin (as GPIO)*/
	gpio_configure_pin(PIN_MODEM_JDR_IDX, PIN_MODEM_JDR_FLAGS);
#endif

#ifdef CONF_USART1_TEST_EN
	sysclk_enable_peripheral_clock(ID_USART1);

	/* Configure USART RXD pin */
	gpio_configure_pin(PIN_USART1_RXD_IDX, PIN_USART1_RXD_FLAGS);
	/* Configure USART TXD pin */
	gpio_configure_pin(PIN_USART1_TXD_IDX, PIN_USART1_TXD_FLAGS);
	/* Configure USART CTS pin */
	gpio_configure_pin(PIN_USART1_CTS_IDX, PIN_USART1_CTS_FLAGS);
	/* Configure USART RTS pin */
	gpio_configure_pin(PIN_USART1_RTS_IDX, PIN_USART1_RTS_FLAGS);
	
	/* Configure USART DTR pin (as GPIO)*/
	gpio_configure_pin(PIN_USART1_DTR_IDX, PIN_USART1_DTR_FLAGS);
	
	/* Configure XBee pow on pin (as GPIO)*/
	gpio_configure_pin(PIN_XBEE_ON_IDX, PIN_XBEE_ON_FLAGS);		
#endif

#ifdef CONF_USART2_TEST_EN
	sysclk_enable_peripheral_clock(ID_USART2);

	/* Configure USART RXD pin */
	gpio_configure_pin(PIN_USART2_RXD_IDX, PIN_USART2_RXD_FLAGS);
	/* Configure USART TXD pin */
	gpio_configure_pin(PIN_USART2_TXD_IDX, PIN_USART2_TXD_FLAGS);
	/* Configure USART CTS pin */
	gpio_configure_pin(PIN_USART2_CTS_IDX, PIN_USART2_CTS_FLAGS);
	/* Configure USART RTS pin */
	gpio_configure_pin(PIN_USART2_RTS_IDX, PIN_USART2_RTS_FLAGS);
#endif



#ifdef CONF_USB_TEST_EN
	/* Configure USB_ID (UOTGID) pin */
	gpio_configure_pin(USB_ID_GPIO, USB_ID_FLAGS);
#endif

#ifdef CONF_CAN0_TEST_EN
	pmc_enable_periph_clk(ID_CAN0);

	/* Configure the CAN0 TX and RX pins. */
	gpio_configure_pin(PIN_CAN0_RX_IDX, PIN_CAN0_RX_FLAGS);
	gpio_configure_pin(PIN_CAN0_TX_IDX, PIN_CAN0_TX_FLAGS);
#endif

#ifdef CONF_EMAC_TEST_EN
	pmc_enable_periph_clk(ID_EMAC);
	gpio_configure_pin(PIN_EMAC_EREFCK, PIN_EMAC_EREFCK_FLAGS);
	gpio_configure_pin(PIN_EMAC_ETX0, PIN_EMAC_ETX0_FLAGS);
	gpio_configure_pin(PIN_EMAC_ETX1, PIN_EMAC_ETX1_FLAGS);
	gpio_configure_pin(PIN_EMAC_ETXEN, PIN_EMAC_ETXEN_FLAGS);
	gpio_configure_pin(PIN_EMAC_ECRSDV, PIN_EMAC_ECRSDV_FLAGS);
	gpio_configure_pin(PIN_EMAC_ERX0, PIN_EMAC_ERX0_FLAGS);
	gpio_configure_pin(PIN_EMAC_ERX1, PIN_EMAC_ERX1_FLAGS);
	gpio_configure_pin(PIN_EMAC_ERXER, PIN_EMAC_ERXER_FLAGS);
	gpio_configure_pin(PIN_EMAC_EMDC, PIN_EMAC_EMDC_FLAGS);
	gpio_configure_pin(PIN_EMAC_EMDIO, PIN_EMAC_EMDIO_FLAGS);
	
	/* Configure INT pin */
	gpio_configure_pin(PIN_ETH_INT, PIN_ETH_INT_FLAGS);
	
	/* Configure reset pin */
	gpio_configure_pin(PIN_ETH_RESET, PIN_ETH_RESET_FLAGS);
#endif

#ifdef CONF_SD_MMC_TEST_EN
	pmc_enable_periph_clk(ID_HSMCI);
	pmc_enable_periph_clk(ID_DMAC);
	/* Configure HSMCI pins */
	gpio_configure_pin(PIN_HSMCI_MCCDA_GPIO, PIN_HSMCI_MCCDA_FLAGS);
	gpio_configure_pin(PIN_HSMCI_MCCK_GPIO, PIN_HSMCI_MCCK_FLAGS);
	gpio_configure_pin(PIN_HSMCI_MCDA0_GPIO, PIN_HSMCI_MCDA0_FLAGS);
	gpio_configure_pin(PIN_HSMCI_MCDA1_GPIO, PIN_HSMCI_MCDA1_FLAGS);
	gpio_configure_pin(PIN_HSMCI_MCDA2_GPIO, PIN_HSMCI_MCDA2_FLAGS);
	gpio_configure_pin(PIN_HSMCI_MCDA3_GPIO, PIN_HSMCI_MCDA3_FLAGS);

	/* Configure SD/MMC card detect pin */
	gpio_configure_pin(SD_MMC_0_CD_GPIO, SD_MMC_0_CD_FLAGS);
#endif
}
