/*
 * tests.c
 * 
 * Created: 30.10.2013 14:19:50
 *  Author: Krasutski Denis
 */ 
#include <asf.h>
#include "board.h"
#include "conf_board.h"
#include "tests.h"
#include "gpio_control.h"
#include "string.h"

#include "ethernet_phy.h"
#include "emac.h"

#include "uip.h"
#include "uip_arp.h"

#include "fat_filelib.h"


#ifdef CONF_LED_TEST_EN
/*! \brief Run test LEDs.
 *
 * \param none.
 *
 * \note Just blinking.
 */
void Test_leds(void)
{
	
	TESTLOG("\t-=Leds Test=-\r\n");
	TESTLOG("Change state Leds to GREEN-ORANGE-RED...\r\n");

	LED_Off(LED1_GPIO);	// disable red led
	LED_On(LED0_GPIO);	// enabled green led
		
	TESTLOG("Green - ON, press key...\r\n");
	TEST_GETCHAR();
		
	LED_On(LED1_GPIO);	// enabled red led
	
	TESTLOG("Orange - ON, press key...\r\n");
	TEST_GETCHAR();
	
	LED_Off(LED0_GPIO);	// disable green 
	
	TESTLOG("Red - ON, press key...\r\n");
	TEST_GETCHAR();

	LED_Off(LED1_GPIO);	// disable red led
	LED_On(LED0_GPIO);	// enabled green led
	
	TESTLOG("Now again GREEN\r\n");
	TESTLOG("End test\r\n");		
}
#endif


#ifdef CONF_TWI1_TEST_EN

/*! \brief Run test I2C.
 *
 * \param none.
 *
 * \note Found on I2C bus devices, and write data to eeprom (24lc16).
 */
void Test_EEPROM(void)
{
	volatile uint8_t data[EEPROM_BLOCK_SIZE];
	volatile uint32_t i,j, n=0;
	
	twi_master_options_t opt = {
		.speed = 40000,
		.chip  = 0x50
	 };
	
	twi_enable_master_mode(BOARD_AT24C_TWI_INSTANCE);
	twi_master_init(BOARD_AT24C_TWI_INSTANCE, &opt);
	TESTLOG("\t-=I2C Test=-\r\n");

	TESTLOG("Scan I2C devices...\r\n");
	j=0;//device count
	for(i=1; i<128; i++)
	{
		if (twi_probe(BOARD_AT24C_TWI_INSTANCE, i) == TWI_SUCCESS )
		{			
			TESTLOG("Ask from address-0x%02X\r\n",(unsigned int)i);
			j++;
		}
	}	
	TESTLOG("Scan address complete, founded device-%d\r\n", (unsigned int)j);
	
	if(j == 0)
	{
		TESTLOG("End test\r\n");	
		return;
	}
	
	TESTLOG("Start test 24lc16 eeprom write/read/compare\r\n");
	TESTLOG("Write data(%d bytes):\r\n",EEPROM_BLOCK_SIZE);
	for (i=0; i<(EEPROM_BLOCK_SIZE / EEPROM_PAGE_SIZE); i++)
	{	
		for(j=0; j<EEPROM_PAGE_SIZE; j++)
		{
			data[j]=j+i;
			TESTLOG("0x%02X ",data[j]);
		}
		TESTLOG("\r\n");
		at24cxx_write_page(i,EEPROM_PAGE_SIZE,(uint8_t*)data);
		delay_ms(30);
	}

	memset((uint8_t*)data, 0, EEPROM_BLOCK_SIZE);
	TESTLOG("\r\n");
	
	at24cxx_read_continuous(0, EEPROM_BLOCK_SIZE,(uint8_t*)data);
	 
	TESTLOG("Read data(%d bytes):\r\n", EEPROM_BLOCK_SIZE);
	for (i=0; i<(EEPROM_BLOCK_SIZE / EEPROM_PAGE_SIZE); i++)
	{	
		for(j=0; j<EEPROM_PAGE_SIZE; j++)
		{
			if( data[i*EEPROM_PAGE_SIZE+j] != (j+i) )
			{
				n++;
			}
			TESTLOG("0x%02X ", data[i*EEPROM_PAGE_SIZE+j]);
		}
		TESTLOG("\r\n");
	}
	
	if(n>0)
		TESTLOG("Errors-%d\r\n", (unsigned int)n);
	else
		TESTLOG("No errors, Ok\r\n");

	TESTLOG("End test\r\n");
}
#endif

#ifdef CONF_RELAY_TEST_EN
/*! \brief Run test I2C.
 *
 * \param none.
 *
 * \note turn on RELAYs outputs.
 */
void Test_Relay(void)
{			
	TESTLOG("\t-=Relays Test=-\r\n");

	TESTLOG("Relay#0 -On\r\n");
	Relay_On(RELAY0_GPIO);
		
	TESTLOG("for test next relay press any key...\r\n");
	TEST_GETCHAR();
	
	Relay_Off(RELAY0_GPIO);
	TESTLOG("Relay#0 -Off\r\n");
	

	TESTLOG("Relay#1 -On\r\n");
	Relay_On(RELAY1_GPIO);
	
	TESTLOG("for test next relay press any key...\r\n");
	TEST_GETCHAR();

	Relay_Off(RELAY1_GPIO);
	TESTLOG("Relay#1 -Off\r\n");
		

	TESTLOG("Relay#2 -On\r\n");
	Relay_On(RELAY2_GPIO);

	TESTLOG("for test next relay press any key...\r\n");
	TEST_GETCHAR();

	Relay_Off(RELAY2_GPIO);
	TESTLOG("Relay#2 -Off\r\n");	
		
	TESTLOG("Relay#3 -On\r\n");
	Relay_On(RELAY3_GPIO);

	TESTLOG("for end test press any key...\r\n");
	TEST_GETCHAR();

	Relay_Off(RELAY3_GPIO);
	TESTLOG("Relay#3 -Off\r\n");

	TESTLOG("End test\r\n");
}
#endif


#ifdef CONF_USART1_TEST_EN

/*! \brief Test XBee pins Rx\TX PW_on DTR
 *
 * \param none.
 *
 */
void Test_Xbee(void)
{
	const sam_usart_opt_t usart_console_settings = {
		115200,
		US_MR_CHRL_8_BIT,
		US_MR_PAR_NO,
		US_MR_NBSTOP_1_BIT,
		US_MR_CHMODE_NORMAL
	};
	
	uint8_t key;
	
	TESTLOG("\t-=XBee test=-\r\n");
	TESTLOG("Init data chennel for XBee on speed 115200...\r\n");

	usart_init_rs232(USART_XBEE, &usart_console_settings,
		sysclk_get_main_hz());
	usart_enable_tx(USART_XBEE);
	usart_enable_rx(USART_XBEE);
	
	TESTLOG("Power-On\r\n");
	USART_XBEE_DTR_ON(); //enabled for test
	XBEE_POWER_ON();

	do
	{
		TESTLOG("Send test sequence...Ok\r\n");
		usart_write_line(USART_XBEE,"TestDataForXBeeModule");
		TESTLOG("For repeat test sequance press SPACE or any key for end test...\r\n");
		key=TEST_GETCHAR();
	}while(key == 0x20/*space*/);
	
	USART_XBEE_DTR_OFF();
	XBEE_POWER_OFF();
	TESTLOG("Power-Off\r\n");
	
	TESTLOG("End test\r\n");
}
#endif

static uint32_t wait_usart_answer(Usart * usart, volatile uint8_t *buff, volatile uint32_t size, volatile uint32_t timeout_us )
{
	volatile uint32_t i=0;
	volatile uint32_t char_buf;
	volatile const uint32_t delay = 1;
	volatile uint32_t time = 0;
	
	while( i < size )
	{
		if (usart_read(usart, &char_buf) == 0)
		{
			buff[i++] = (uint8_t)char_buf;
		}
		delay_us(delay);
		time++;
		if(timeout_us < time)
			break;
	}
	return i;
}


#ifdef CONF_USART0_TEST_EN

/*! \brief Test GSM modem, getting version and send SMS
 *
 * \param none.
 *
 */
void Test_Modem(void)
{
	const sam_usart_opt_t usart_console_settings = {
		115200,
		US_MR_CHRL_8_BIT,
		US_MR_PAR_NO,
		US_MR_NBSTOP_1_BIT,
		US_MR_CHMODE_NORMAL
	};
	
	uint8_t ask_buf[32];
	uint8_t phone_num[16] = "+375297762669";
	uint8_t sms_text[128] = "IRBIS\rTest message";
	uint8_t send_buffer[256];
	uint32_t i;
	uint32_t timeout = 500*1000; //in us
	uint8_t key;
	
	
	TESTLOG("\t-=Modem Test=-\r\n");
	
	TESTLOG("Init data channel for Modem on 115200...\r\n");

	///Init RS232
	usart_init_rs232(USART_GSM, &usart_console_settings,
		sysclk_get_main_hz());
	usart_enable_tx(USART_GSM);
	usart_enable_rx(USART_GSM);
	delay_ms(1);
	
	TESTLOG("Modem power - On\r\n");
	MODEM_POWER_ON();		///Modem power on
	delay_ms(500);			///wait reset
	
#ifndef MODEM_WITHOUT_RESET	
	TESTLOG("Modem Reset...wait\r\n");
	MODEM_RESET_ON();		///Reset
	delay_ms(1500);			///wait reset
	MODEM_RESET_OFF();		///Reset off	
	delay_ms(500);			///wait reset
	TESTLOG("Modem Reset...Ok\r\n");
#endif
	///wait 2s for startup modem
	delay_ms(2000);
	TESTLOG("Send status request...wait\r\n");

	///Getting Firmware version
	///clear receive buffer
	memset(ask_buf, 0, sizeof(ask_buf));
	///send request
	usart_write_line(USART_GSM,"AT+CGMR\r");
	///waint answer
	i=wait_usart_answer(USART_GSM, ask_buf, 25/*OK - lenght*/, timeout);
	///check answer and print version
	if(i>0)
		if (strstr((const char *)ask_buf, (const char *)"OK") != 0)
			TESTLOG("Modem Firmware version:%s\r\n",strstr((const char *)ask_buf, (const char *)"1"));
		
	
	///Set text sms mode
	memset(ask_buf, 0, sizeof(ask_buf));
	delay_ms(500);
	usart_write_line(USART_GSM,"AT+CMGF=1\r");    
	wait_usart_answer(USART_GSM, ask_buf, 32/*OK - lenght*/, timeout);
	
	if (strstr((const char *)ask_buf, (const char *)"OK") == 0)
	{
		if (strstr((const char *)ask_buf,  (const char *)"ERROR") != 0)
		{
			TESTLOG("Modem detected.\r\n");
			TESTLOG("Bad answer, check SIM card or Antenna\r\n");
			goto clean_up;
		}
		else
		{
			TESTLOG("Modem not found.\r\n");
			TESTLOG("No answer from modem :( \r\n");
			goto clean_up;
		}
	}

	///Select TE Character Set 
	usart_write_line(USART_GSM, "AT+CSCS=\"IRA\"\r");
	
	TESTLOG("Modem ready for send SMS.\r\n");
	do
	{
		TESTLOG("Enter phone number:\r\n");	
		i=0;
		do 
		{
			key=TEST_GETCHAR();
			if( ( (key>='0') && (key<='9') ) || (key == '+'))
			{
				TEST_WRITECHAR(key);
				phone_num[i++]=key;
			}	
		}while (key != 0x0D);
		
		phone_num[i]=0;
		TESTLOG("\r\n");
		TESTLOG("Phone:%s ?[Y/N]\r\n",phone_num);
		do
		{
			key=TEST_GETCHAR();
		}while((key != 'Y') && (key != 'y') && (key != 'N') && (key != 'n'));
		TEST_WRITECHAR(key);
	}while ((key != 'Y') && (key != 'y') );


	TESTLOG("Enter sms test:\r\n");	
	i=0;
	do 
	{
		key=TEST_GETCHAR();
		if( key !=0x7f/*backspace*/)
		{
			TEST_WRITECHAR(key);
			sms_text[i++]=key;
		}	
		else
		{
			TEST_WRITECHAR(key);
			sms_text[i--]=0;
		}
		
	}while (key != 0x0D);
	sms_text[i]=0;
	TESTLOG("\r\n");
	
	TESTLOG("SMS:%s\r\n",sms_text);
	TESTLOG("Sending message...\r\n");

	sprintf(send_buffer, "AT+CMGS=%s\r",phone_num);
	usart_write_line(USART_GSM,(const char*)send_buffer);// send the SMS the number
	delay_ms(1500);//wait symbol '>'
	usart_write_line(USART_GSM, (const char*)sms_text); // send Message
	delay_ms(500);
	usart_write(USART_GSM, 0x1A); // start send
	
	memset(ask_buf, 0, sizeof(ask_buf));
	wait_usart_answer(USART_GSM, ask_buf, 32/*OK - lenght*/, timeout*5);
	
	if (strstr((const char *)ask_buf, (const char *)"OK") != 0)
	{
		TESTLOG("SMS sent!\r\n");
	}
	else
	{
		TESTLOG("Error!\r\n");		
	}
	
clean_up:
	///Modem power off
	MODEM_POWER_OFF();		
	TESTLOG("End test\r\n");
}
#endif

#ifdef CONF_USART2_TEST_EN
/*! \brief Test RS485 interface, send via interface following text "TestRS485"
 *
 * \param none.
 *
 */
void Test_RS485(void)
{
	const sam_usart_opt_t usart_console_settings = {
		9600,
		US_MR_CHRL_8_BIT,
		US_MR_PAR_NO,
		US_MR_NBSTOP_1_BIT,
		US_MR_CHMODE_NORMAL
	};
	uint8_t key;
	
	TESTLOG("\t-=RS485 bus Test=-");
	TESTLOG("Init rs485 on speed 9600...\r\n");

	usart_init_rs485(USART_RS485, &usart_console_settings,
		sysclk_get_main_hz());
	usart_enable_tx(USART_RS485);
	usart_enable_rx(USART_RS485);
	
	do
	{
		TESTLOG("Send test sequence...Ok\r\n");
		usart_write_line(USART_RS485,"TestRS485\r\n");
		TESTLOG("For repeat test sequance press SPACE or any key for end test...\r\n");
		key=TEST_GETCHAR();
	}while(key == 0x20/*space*/);
	
	TESTLOG("End test\r\n");
}
#endif

#ifdef CONF_CAN0_TEST_EN
/*! \brief Test CAN interface, send via interface following data: 0x12345678 0x87654321
 *
 * \param none.
 *
 */
void Test_CAN(void)
{
	 can_mb_conf_t can0_mailbox;
	 uint8_t key;
	 uint32_t can_speed = CAN_BPS_10K;

	TESTLOG("\t-=CAN Test=-\r\n");
	TESTLOG("Init CAN0 channel on speed %dK...\r\n",(int)can_speed);


	 can_init(CAN0, sysclk_get_main_hz(), can_speed);

	 can_reset_all_mailbox(CAN0);

	 can0_mailbox.ul_mb_idx = 0;
	 can0_mailbox.uc_obj_type = CAN_MB_TX_MODE;
	 can0_mailbox.uc_tx_prio = 15;
	 can0_mailbox.uc_id_ver = 0;
	 can0_mailbox.ul_id_msk = 0;
	 can_mailbox_init(CAN0, &can0_mailbox);

	 can0_mailbox.ul_id = CAN_MID_MIDvA(0x07);
	 can0_mailbox.ul_datal = 0x12345678;
	 can0_mailbox.ul_datah = 0x87654321;
	 can0_mailbox.uc_length = 8;
	
	 do
	 {
		TESTLOG("Send test sequence...Ok\r\n");
		
		can_mailbox_write(CAN0, &can0_mailbox);
		TESTLOG("For repeat test sequance press SPACE or any key for end test...\r\n");
		key=TEST_GETCHAR();
	 }while(key == 0x20/*space*/);		
		
	TESTLOG("End test\r\n");
}
#endif


#ifdef CONF_SD_MMC_TEST_EN
/*! \brief Implemented read block function for FAT32 libs
 *
 * \param none.
 *
 */
static int media_read(unsigned long sector, unsigned char *buffer) {

	sd_mmc_init_read_blocks(0,sector,1);
	sd_mmc_start_read_blocks(buffer,1);
	while(SD_MMC_OK != sd_mmc_wait_end_of_read_blocks());

	return 1;
}
/*! \brief Implemented write block function for FAT32 libs
 *
 * \param none.
 *
 */
static int media_write(unsigned long sector, unsigned char *buffer) {

	sd_mmc_init_write_blocks(0,sector,1);
	sd_mmc_start_write_blocks(buffer,1);
		while(SD_MMC_OK != sd_mmc_wait_end_of_write_blocks());

	return 1;
}

/*! \brief Test SDcard, print card size and file list in '/' directory
 *
 * \param none.
 *
 */
void Test_SD_MMC(void)
{
	uint32_t status=0;
	FL_DIR dirstat;
	int filenumber = 0;
	uint32 i = 0;
	
	TESTLOG("\t-=SDcard Test=-\r\n");

	TESTLOG("Checking SD card...wait\r\n");	
	sd_mmc_init();

	do
	{
		status = sd_mmc_check(0);
		i++;
		if (i == 5000)
		{
			if(status != SD_MMC_INIT_ONGOING)
			{
				TESTLOG("SD card not inserted!\r\n");
				return;
			}
		}
		if (i == 250000)
		{
			if(status != SD_MMC_INIT_ONGOING)
			{
				TESTLOG("SD card cant init!\r\n");
				return;
			}
		}
	}while(status != SD_MMC_OK);
	
	TESTLOG("SD card datected.\r\n");
	
	TESTLOG("Init file system...\r\n");

	// Initialise File IO Library
	fl_init();

	// Attach media access functions to library
	if (fl_attach_media((fn_diskio_read)media_read, (fn_diskio_write)media_write) != FAT_INIT_OK) {
		TESTLOG("Init file system...FAIL\r\n");
		return;
	}
	
	TESTLOG("Init file system...Ok\r\n");
	TESTLOG("Print file list on SD card:\r\n");
	TESTLOG("\r\n");
	TESTLOG("No.             Filename\r\n");

	if (fl_opendir("/", &dirstat))
	{
		struct fs_dir_ent dirent;
		while (fl_readdir(&dirstat, &dirent) == 0)
		{
			if (dirent.is_dir)
			{
				TESTLOG("%d - %s <DIR> (0x%x)\r\n",++filenumber, dirent.filename, (unsigned int)dirent.cluster);
			}
			else
			{
				TESTLOG("%d - %s [%d bytes] (0x%x)\r\n",++filenumber, dirent.filename, (unsigned int)dirent.size,(unsigned int) dirent.cluster);
			}
		}

		fl_closedir(&dirstat);
		TESTLOG("Looking for files Compleate.Files:%d",filenumber);
	}
	TESTLOG("\r\n");
	
	TESTLOG("End test\r\n");
}
#endif

#ifdef CONF_EMAC_TEST_EN
#define BUF ((struct uip_eth_hdr *)&uip_buf[0])

 static emac_device_t gs_emac_dev;
 static volatile uint8_t gs_uc_eth_buffer[EMAC_FRAME_LENTGH_MAX];
 uint32_t ul_frm_size;
 struct uip_eth_addr mac = { {ETHERNET_CONF_ETHADDR0,
		 ETHERNET_CONF_ETHADDR1, 
		 ETHERNET_CONF_ETHADDR2, 
		 ETHERNET_CONF_ETHADDR3, 
		 ETHERNET_CONF_ETHADDR4, 
		 ETHERNET_CONF_ETHADDR5}  };
static uint8_t gs_uc_mac_address[] =
 { ETHERNET_CONF_ETHADDR0, ETHERNET_CONF_ETHADDR1, ETHERNET_CONF_ETHADDR2,
   ETHERNET_CONF_ETHADDR3, ETHERNET_CONF_ETHADDR4, ETHERNET_CONF_ETHADDR5};		 
  emac_options_t emac_option;
  		 
static void up_serv(void) {
	//init stack
	uip_init();
	uip_arp_init();

	// init HOST port 80
	hello_world_init();

	//set MAC addr
	uip_setethaddr(mac);

	// set IP
	uip_ipaddr_t ipaddr;
	uip_ipaddr(ipaddr, 
		ETHERNET_CONF_IPADDR0, 
		ETHERNET_CONF_IPADDR1, 
		ETHERNET_CONF_IPADDR2,
		ETHERNET_CONF_IPADDR3);
	uip_sethostaddr(ipaddr);
	
	uip_ipaddr(ipaddr, 
		ETHERNET_CONF_GATEWAY_ADDR0, 
		ETHERNET_CONF_GATEWAY_ADDR1, 
		ETHERNET_CONF_GATEWAY_ADDR2, 
		ETHERNET_CONF_GATEWAY_ADDR3);
	uip_setdraddr(ipaddr);
	
	uip_ipaddr(ipaddr, 
		ETHERNET_CONF_NET_MASK0, 
		ETHERNET_CONF_NET_MASK1,
		ETHERNET_CONF_NET_MASK2, 
		ETHERNET_CONF_NET_MASK3);
	uip_setnetmask(ipaddr);
}

void EMAC_Handler(void)
{
        emac_handler(&gs_emac_dev);
}


void Test_Ethernet(void)
{
	/* Max PHY number */
	#define ETH_PHY_MAX_ADDR   31
	
	uint32_t value1 = 0;
	uint32_t value2 = 0;
	uint32_t phy_addr = 0;
	uint8_t delay_arp = 0;
	uint32_t i;

	TESTLOG("\t-=Micrel KSZ8051 phy test=-\r\n");	 
	
	TESTLOG("Reset KSZ8051, set active reset signal...\r\n");	 
	ETH_PHY_RESET_ON();
	delay_ms(500);
	ETH_PHY_RESET_OFF();	
	delay_ms(500);
	TESTLOG("Reset KSZ8051...done.\r\n");	
	
	TESTLOG("Scan MDIO bus...\r\n");	
	emac_enable_management(EMAC, 1);

	for (i = 0; i <= ETH_PHY_MAX_ADDR; i++) 
	{
		emac_phy_read(EMAC, i, EMII_PHYID1, &value1);
		if ( value1 != 0xFFFF)
		{
			emac_phy_read(EMAC, i, EMII_PHYID2, &value2);
			TESTLOG("Found PHY_Addr-%02X ID=0x%04X_%04X\r\n", (unsigned int)i, (unsigned int)value1, (unsigned int)value2);
			phy_addr = i;
		}
	}
	
	TESTLOG("Scan MDIO bus...done.\r\n");	
	
	if(phy_addr == 0)
	{
		TESTLOG("ERROR:Phy not found.\r\n");
		emac_enable_management(EMAC, 0);
		return;
	}
	
	
	/* 
	 * Set the Auto_negotiation Advertisement Register.
	 * MII advertising for Next page.
	 * 100BaseTxFD and HD, 10BaseTFD and HD, IEEE 802.3.
	 */
	value1 = EMII_100TX_FDX | EMII_100TX_HDX | EMII_10_FDX | EMII_10_HDX | 
			EMII_AN_IEEE_802_3;
	emac_phy_write(EMAC, phy_addr, EMII_ANAR, value1);

	/* Read & modify control register */
	emac_phy_read(EMAC, phy_addr, EMII_BMCR, &value1);

	value1 |= EMII_SPEED_SELECT | EMII_AUTONEG | EMII_DUPLEX_MODE;
	emac_phy_write(EMAC, phy_addr, EMII_BMCR, value1);

	/* Restart auto negotiation */
	value1 |= (uint32_t)EMII_RESTART_AUTONEG;
	value1 &= ~(uint32_t)EMII_ISOLATE;
	emac_phy_write(EMAC, phy_addr, EMII_BMCR, value1);
	
	emac_phy_read(EMAC, phy_addr, EMII_PCR2, &value1);
	value1 &= ~(1<<7);//set 25MHZ
	emac_phy_write(EMAC, phy_addr, EMII_PCR2, value1);
	
	TESTLOG("Getting link status...\r\n");	
	/* Check if auto negotiation is completed */
	for(i=0; i<100; i++)
	 {
		emac_phy_read(EMAC, phy_addr, EMII_BMSR, &value1);

		/* Done successfully */
		if (value1 & EMII_AUTONEG_COMP) {
			break;
		}
	
		delay_ms(20);
	}
	
	emac_phy_read(EMAC, phy_addr, EMII_BMSR, &value1);
	emac_enable_management(EMAC, 0);
	
	if(value1 & EMII_LINK_STATUS)
	{
		TESTLOG("Link status: UP\r\n");
	}else
	{
		TESTLOG("Link status: DOWN\r\n");
		return;
	}	
	////////////////////////
	
	TESTLOG("Start TCP/IP stack:\r\n");
	TESTLOG("IP:%d.%d.%d.%d\r\n", 
		ETHERNET_CONF_IPADDR0,
		ETHERNET_CONF_IPADDR1,
		ETHERNET_CONF_IPADDR2,
		ETHERNET_CONF_IPADDR3);
	TESTLOG("MS:%d.%d.%d.%d\r\n", 
		ETHERNET_CONF_NET_MASK0,
		ETHERNET_CONF_NET_MASK1,
		ETHERNET_CONF_NET_MASK2,
		ETHERNET_CONF_NET_MASK3);
		
	///set emac parameters	
	emac_option.uc_copy_all_frame = 1;
	emac_option.uc_no_boardcast = 0;
	memcpy(emac_option.uc_mac_addr, gs_uc_mac_address, sizeof(gs_uc_mac_address));
	gs_emac_dev.p_hw = EMAC;
	 
	///init emac
	emac_dev_init(EMAC, &gs_emac_dev, &emac_option);
	NVIC_SetPriority(EMAC_IRQn, 2);
	NVIC_EnableIRQ(EMAC_IRQn);
	ethernet_phy_init(EMAC, BOARD_EMAC_PHY_ADDR, sysclk_get_cpu_hz());
	ethernet_phy_auto_negotiate(EMAC, BOARD_EMAC_PHY_ADDR);
	ethernet_phy_set_link(EMAC, BOARD_EMAC_PHY_ADDR, 1);
	
	///run uip stack
	up_serv();
	
	TESTLOG("TCP/IP stack is run\r\n");
	TESTLOG("use ping from PC for check it\r\n");
	TESTLOG("e.g. >ping 192.168.1.180 \r\n");
	TESTLOG("also you should be see web page in browser\r\n");
	TESTLOG("e.g. http://192.168.1.180 \r\n");
	TESTLOG("FOR EXIT FROM THIS TEST REBOOT DEVICE\r\n");
	
	///listen 80 port, and ARP send
	while (1)
	{
		///Read receive data
		emac_dev_read(&gs_emac_dev,	(uint8_t *) uip_buf, UIP_BUFSIZE, (uint32_t *)&uip_len);
		if (uip_len > 0) 
		{
			if (BUF->type == htons(UIP_ETHTYPE_IP)) 
			{
				uip_arp_ipin();
				uip_input();
				if (uip_len > 0) 
				{
					uip_arp_out();
					emac_dev_write(&gs_emac_dev,uip_buf,uip_len,NULL);
				}
			} 
			else if (BUF->type == htons(UIP_ETHTYPE_ARP)) 
			{
				uip_arp_arpin();
				if (uip_len > 0) 
				{
					emac_dev_write(&gs_emac_dev,uip_buf,uip_len,NULL);
				}
			}
		}
		
		delay_arp++;
		for (i = 0; i < UIP_CONNS; i++)
		{
			uip_periodic(i);
			if (uip_len > 0)
			{
				uip_arp_out();
				emac_dev_write(&gs_emac_dev,uip_buf,uip_len,NULL);
			}
		}

		uip_arp_timer();
	}
	
	TESTLOG("End test\r\n");
}
#endif


#ifdef CONF_DIG_TEST_EN
/*! \brief Print digit pin state
 *
 * \param none.
 *
 */
void Test_DigitPin(void)
{
	TESTLOG("-=Count inputs test=-\r\n");
	TESTLOG("Read pins value:\r\n");
	uint8_t key;
	do 
	{
		TESTLOG("C1 - %01X ", (unsigned int)pio_get_pin_value(COUNT1_GPIO));
		TESTLOG("C2 - %01X ", (unsigned int)pio_get_pin_value(COUNT2_GPIO));
		TESTLOG("C3 - %01X ",(unsigned int) pio_get_pin_value(COUNT3_GPIO));
		TESTLOG("C4 - %01X ",(unsigned int) pio_get_pin_value(COUNT4_GPIO));
		TESTLOG("C5 - %01X ", (unsigned int)pio_get_pin_value(COUNT5_GPIO));
		TESTLOG("C6 - %01X ", (unsigned int)pio_get_pin_value(COUNT6_GPIO));
		TESTLOG("\r\n");
		TESTLOG("For repeat read COUNTx pins state press SPACE or any key for end test...\r\n");
		key=TEST_GETCHAR();
	}while(key == 0x20/*space*/);
	
	TESTLOG("End test\r\n");
}
#endif

#ifdef CONF_ANALOG_TEST_EN
/*! \brief Print analog pins state
 *
 * \param none.
 *
 */
void Test_AnalPin(void)
{
	#define ADC_CLOCK 1000 //HZ
	enum adc_channel_num_t adc_list[]={
		ADC_CHANNEL_0,
		ADC_CHANNEL_1,
		ADC_CHANNEL_2,
		ADC_CHANNEL_3,
		ADC_CHANNEL_7,
		ADC_CHANNEL_10,
	};
	uint32_t adc_value[6];
	uint32_t i;
	uint8_t key;

	TESTLOG("\t-=ADC test=-\r\n");

	///Init ADC
	adc_init(ADC, sysclk_get_main_hz(), ADC_CLOCK, 8);
    adc_configure_timing(ADC, 0, ADC_SETTLING_TIME_3, 1);
    adc_set_resolution(ADC, ADC_MR_LOWRES_BITS_12);
	
	///Init channels
	for(i=0; i< sizeof(adc_list); i++)
	{
		adc_enable_channel(ADC, adc_list[i]);	
	}    
	
    adc_configure_trigger(ADC, ADC_TRIG_SW, 1);
	adc_disable_interrupt(ADC,0xffffffff);
	
	///Start 
	adc_start(ADC);
	
	do
	{
		///get adc value for all channels
		for(i=0; i< sizeof(adc_list); i++)
		{
			while( (adc_get_channel_status(ADC, adc_list[i])& ADC_ISR_DRDY)
				== ADC_ISR_DRDY);
			adc_value[i] = adc_get_channel_value(ADC, adc_list[i]);
		}
		
		///print adc value for all channels
		for(i=0; i< sizeof(adc_list); i++)
		{
			TESTLOG("A%d-0x%03X ",(int)i, (unsigned int)adc_value[i]);
		}
			
		TESTLOG("\r\n");
	
		TESTLOG("For get ADC value again press SPACE or any key for end test...\r\n");
		key=TEST_GETCHAR();
	 }while(key == 0x20/*space*/);		
	
	adc_stop(ADC);
	TESTLOG("End test\r\n");
}
#endif

#ifdef CONF_USB_FLASH
/*! \brief run USB bootloder
 *
 * \param none.
 *
 */
void USB_BootLoader(void)
{

#define CMD_EA		0x05 /*< Erase all command*/
#define	CMD_SGPB	0x0B /*< Set GPNVM Bit command */
#define	CMD_CGPB	0x0C /*< Clear GPNVM Bit command */
uint32_t key;

	TESTLOG("\t-=ATSAM3X ReFlash=-\r\n");
	
	TESTLOG("For switch device to USB SAM-BA loader press \'y\' key\r\n");
	TESTLOG("\'y\' or Any key to exit...\r\n");
	key=TEST_GETCHAR();
	
	if (key == 'y' || key == 'Y')
	{
		TESTLOG("Okay, now reboot device!\r\n");
		
		delay_ms(500);

		///Init EFC
		efc_init(EFC0, 0,20);
	
		///Set command for switch boot Memory (to IROM)
		efc_perform_command(EFC0, CMD_CGPB, 1);
	
	}

}
#endif