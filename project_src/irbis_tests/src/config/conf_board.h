/**
 * \file
 *
 * \brief Board configuration.
 *
 * Copyright (c) 2011 - 2013 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef CONF_BOARD_H_INCLUDED
#define CONF_BOARD_H_INCLUDED

/* Enable LED pins */
#define CONF_LED_TEST_EN

/* Enable COUNTx pins */
#define CONF_DIG_TEST_EN

/* Enable COUNTx pins */
#define CONF_ANALOG_TEST_EN

/* Enable USB interface */
#define CONF_USB_TEST_EN

/* Enable Relays test */
#define CONF_RELAY_TEST_EN

/* Enable SD cart test */
#define CONF_SD_MMC_TEST_EN

/* Enable EMAC test */
#define CONF_EMAC_TEST_EN

/* Enable CAN0 */
#define CONF_CAN0_TEST_EN

/* Enable TWI(I2C) - 24lc16 test*/
#define CONF_TWI1_TEST_EN

/* Enable USART0 - GSM test*/
#define CONF_USART0_TEST_EN

/* Enable USART1 - Xbee test*/
#define CONF_USART1_TEST_EN

/* Enable USART2 and RS485 test*/
#define CONF_USART2_TEST_EN

/* Enable reflash firmware*/
#define CONF_USB_FLASH


#endif /* CONF_BOARD_H_INCLUDED */
