/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
#include <asf.h>
#include <stdio.h>
#include "conf_board.h"
#include "tests.h"
#include "gpio_control.h"

///structure for menu
typedef struct {
	uint8_t key;
	void (*func)(void);
	const uint8_t* menu_str;
}menu_t;


///Main menu for tests
menu_t test_menu[] =
{
#ifdef CONF_LED_TEST_EN
	{'1', Test_leds,	(const uint8_t*)"Leds Test"},
#endif
#ifdef CONF_RELAY_TEST_EN
	{'2', Test_Relay,	(const uint8_t*)"Relay test"},
#endif
#ifdef CONF_CAN0_TEST_EN	
	{'3', Test_CAN,		(const uint8_t*)"CAN test (only send data)"},
#endif
#ifdef CONF_USART0_TEST_EN
	{'4', Test_RS485,	(const uint8_t*)"RS485 test (only send data)"},
#endif
#ifdef CONF_USART0_TEST_EN		
	{'5', Test_Xbee,	(const uint8_t*)"XBee module test (only send data)"},
#endif
#ifdef CONF_SD_MMC_TEST_EN		
	{'6', Test_SD_MMC,	(const uint8_t*)"SD card test"},
#endif
#ifdef CONF_USART0_TEST_EN		
	{'7', Test_Modem,	(const uint8_t*)"Telit GSM modem test"},
#endif
#ifdef CONF_TWI1_TEST_EN
	{'8', Test_EEPROM,	(const uint8_t*)"TWI(I2C) and eeprom test"},
#endif
#ifdef CONF_ANALOG_TEST_EN
	{'9', Test_AnalPin,	(const uint8_t*)"Analog pins test"},
#endif
#ifdef CONF_DIG_TEST_EN
	{'0', Test_DigitPin,	(const uint8_t*)"Digital pins test"},
#endif
#ifdef CONF_EMAC_TEST_EN
	{'e', Test_Ethernet,(const uint8_t*)"Ethernet Test"},
#endif
#ifdef CONF_USB_FLASH
	{'f', USB_BootLoader,(const uint8_t*)"Flash ATSAM3X"},
#endif
	//{'-', 0, (const uint8_t*)"-----"}
};

#define TEST_MENU_SIZE (sizeof(test_menu) / sizeof(test_menu[0]) )


int _read(int file, char *ptr, int len);
int _write(int file, char *ptr, int len);


#ifdef  CONF_USB_TEST_EN
static bool my_flag_autorize_cdc_transfert = false;
 
 /*! \brief USB callback, when USB connection enable.
 *
 * \param none
 *
 */
 bool my_callback_cdc_enable(void)
 {
    my_flag_autorize_cdc_transfert = true;
    return true;
 }
 
 /*! \brief USB callback, when USB connection disable.
 *
 * \param none
 *
 */
 void my_callback_cdc_disable(void)
 {
    my_flag_autorize_cdc_transfert = false;
 }

/*! \brief wait usb connection.
 *
 * \param none
 *
 * \note Until no connection, LEDs blink (red-orange)
 */
static void wait_usb_connect(void)
{
	if (my_flag_autorize_cdc_transfert == true)
	{
		return;
	}
	
	while(my_flag_autorize_cdc_transfert != true)
	{
		delay_ms(250);
		LED_RED();
		delay_ms(250);
		LED_ORANGE();
	}
	
	LED_GREEN();
}

#endif

/*! \brief Low level write symbol (required for iprintf, newstd).
 *
 * \param file Stdout file
 * \param ptr Pointer on data for send.
 * \param len Data len. 
 *
 * Return Count write data
 */
int _write(int file, char *ptr, int len)
{
#ifdef CONF_USB_TEST_EN	
	int i;
	switch(file)
	{
		case 1:
			if(my_flag_autorize_cdc_transfert == false)
				return 0;
			for(i=0; i<len; i++)
			udi_cdc_putc((ptr[i]));
		default:
			return 0;
	}
#endif	
	return len;
}

/*! \brief Low level read symbol (required for iscanf, newstd).
 *
 * \param file Stdout file
 * \param ptr Pointer on buffer data.
 * \param len Data len. 
 *
 * Return Count read data
 */
int _read(int file, char *ptr, int len)
{
	return 0; //scanf not work!!!!! we use TEST_GETCH()
#ifdef CONF_USB_TEST_EN		
	switch(file)
	{
		case 0:
			if(my_flag_autorize_cdc_transfert == false)
				return 0;
			//for(i=0; i<len; i++)
			*(ptr++) = udi_cdc_getc();
				return 1;
		default:
			return 0;
	}
#endif	
	return len;
}



int main (void)
{
	uint32_t i;
	uint8_t key;	

	///Init pins and clocks
	board_init();

#ifdef CONF_USB_TEST_EN
	///Enabled USB CDC
	udc_start();
	///wait connecting USB
	wait_usb_connect();

	///wait connect via uart, for start press enter 
	do
	{
		LED_GREEN();
		key = TEST_GETCHAR();
		LED_ORANGE();
		delay_ms(250);
		if(key!= 0x0d)
				TESTLOG("press enter to start\r\n");	
	}while(key!=0x0d);	
	LED_GREEN();	
#endif	
	
	TESTLOG("\t-=Board menu test=-\r\n");			
	
	while(1)
	{
#ifdef CONF_USB_TEST_EN	
	///recheck usb connect	
	wait_usb_connect();
#endif
		//print main menu
		TESTLOG("List test:\r\n");
		for(i=0; i<TEST_MENU_SIZE; i++)
		{
			TESTLOG("  %c) %s\r\n",test_menu[i].key, test_menu[i].menu_str);
		}
		TESTLOG("Please enter test:\r\n");
		key = TEST_GETCHAR();
		TESTLOG("%c\r\n",key);
		
		///check and run sub menu
		for(i=0; i<TEST_MENU_SIZE; i++)
		{
			if (test_menu[i].key == key)
			{				
				TESTLOG("-------------------------------------\r\n");
				if(test_menu[i].func != NULL)
					test_menu[i].func();
				TESTLOG("-------------------------------------\r\n");
				break;
			}
		}
		if(i == TEST_MENU_SIZE)
			TESTLOG("BAD key - %c, use key from list!\r\n", key);		
	}
	
	//while end;
	while(1);
}
